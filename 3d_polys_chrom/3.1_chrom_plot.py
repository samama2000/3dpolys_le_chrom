import sys
import os
import glob
import re
import json
import numpy as np
import pandas as pd
import cooler
import matplotlib.pyplot as plt
import chromosight.utils.detection as cud
import scipy.ndimage as ndi
import chromosight.kernels as ck
import scipy.stats as st
import seaborn as sns
import warnings

# Variables
plot_path = sys.argv[1]
cool_path = sys.argv[2]


# Load functions
def load_tsv_files(directory, suffix):
    search_pattern = os.path.join(directory, f'*{suffix}.tsv')
    
    file_list = glob.glob(search_pattern)
    
    if file_list:
        return pd.read_csv(file_list[0], sep='\t')
    else:
        print(f'No {suffix} file found in {directory}')
        return None
    
def load_npy_files(directory, suffix):
    search_pattern = os.path.join(directory, f'*{suffix}.npy')
    
    file_list = glob.glob(search_pattern)
    
    if file_list:
        return np.load(file_list[0])
    else:
        print(f'No {suffix} file found in {directory}')
        return None

# Load the TSV files
loops = load_tsv_files(plot_path, 'loops')
borders = load_tsv_files(plot_path, 'borders')

# Load Hi-C data in cool format
c = cooler.Cooler(cool_path)

# Plot the whole matrix
plt.figure(figsize=(10, 10))
mat = c.matrix(sparse=False, balance=True)[:]
plt.imshow(mat ** 0.2, cmap='afmhot_r')
plt.scatter(loops.bin2, loops.bin1, edgecolors='blue', facecolors='none', label='loops')
plt.scatter(borders.bin2, borders.bin1, c='lightblue', label='borders')
plt.legend()
plt.savefig(f'{plot_path}hic_matrix.pdf')
plt.close()


# Compute loop size (i.e. anchor distance) and plot
get_sizes = lambda df: np.abs(df.start2 - df.start1)
loops['sizes'] = get_sizes(loops)

sns.violinplot(data=loops, y='score')
plt.ylabel('chromosight loop score')
plt.title('Loop score')
plt.axhline(0, c='grey')
plt.savefig(f'{plot_path}loop_score.pdf')
plt.close()

# Compute border size (i.e. anchor distance) and plot
borders['sizes'] = get_sizes(borders)

sns.violinplot(data=borders, y='score')
plt.ylabel('border score')
plt.title('border score')
plt.axhline(0, c='grey')
plt.savefig(f'{plot_path}border_score.pdf')
plt.close()

# Compute histogram
plt.figure(figsize=(8, 8))
fig, ax = plt.subplots(2, 1, sharex=True)
for i, (df, pat) in enumerate(zip([loops, borders], ['loops', 'borders'])):
    ax[i].hist(df.score, 20)
    ax[i].set_title(pat)
plt.tight_layout()
plt.savefig(f'{plot_path}histogram.pdf')
plt.close()
