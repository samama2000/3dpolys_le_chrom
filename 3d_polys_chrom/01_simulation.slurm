#!/bin/bash

# Variables
condition_file="$1"
template_file="$2"
output_dir="$3"

# Check if the correct number of arguments is provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 condition.csv template.txt output_dir/"
    exit 1
fi

# Check if the condition files exist
if [ ! -f "$condition_file" ]; then
    echo "Error: condition file '$condition_file' not found."
    exit 1
fi

if [ ! -f "$template_file" ]; then
    echo "Error: Template file '$template_file' not found."
    exit 1
fi

# Check if the output directory exists, if not, create it
if [ ! -d "$output_dir" ]; then
    mkdir -p "$output_dir"
fi

recomb_file="recombination.csv"
touch ./recombination.csv



# Read variable names from the first row of the condition CSV file
IFS=',' read -ra variables < <(head -n 1 "$condition_file")

# Function to generate combinations recursively
function generate_combinations {
    local var_index=$1
    local prefix="$2"

    if [ "$var_index" -eq "${#variables[@]}" ]; then
        echo "$prefix" >> "$recomb_file"
    else
        local current_variable="${variables[$var_index]}"
        local values=$(awk -F',' -v var_idx=$((var_index + 1)) '{print $var_idx}' "$condition_file" | tail -n +2)
        for value in $values; do
            generate_combinations "$((var_index + 1))" "$prefix$value,"
        done
    fi
}

# Generate combinations
echo "$(IFS=,; echo "${variables[*]}")" > "$recomb_file"
generate_combinations 0 ""

echo "Recombinations processed to '$recomb_file'."

# Read variable names and values from the recomb file
IFS=',' read -ra variables < <(head -n 1 "$recomb_file")

line_number=1

# Creates a simulation for each line in the CSV
tail -n +2 "$recomb_file" | while IFS=',' read -ra values; do
    # Create a unique folder in which to save the simulation
    mkdir -p $output_dir/sim_$line_number
    # Create a unique config file
    output_file="$output_dir/sim_$line_number/config_$line_number.txt"

    # Read the template file line by line and replace variable values
    while IFS= read -r line; do
        # Remove empty lines at the beginning of config files
        if [ -z "$line" ] && [ ! "$config_started" ]; then
            continue
        fi
        config_started=true
        
        for ((i = 0; i < ${#variables[@]}; i++)); do
            variable_name="${variables[$i]}"
            variable_value="${values[$i]}"
            # Escape special characters in variable name and value for sed
            escaped_name=$(sed 's/[&/\]/\\&/g' <<< "$variable_name")
            escaped_value=$(sed 's/[&/\]/\\&/g' <<< "$variable_value")
            # Replace only the right-hand side of the equal sign with the variable value
            line=$(sed "s/^$escaped_name\s*=\s*.*/$escaped_name = $escaped_value/" <<< "$line")
        done
        echo "$line" >> "$output_file"
    done < "$template_file"

    # Convert config file to "input.cfg"
    cfg_file=$output_dir/sim_$line_number/input.cfg
    cp $output_file $cfg_file
    echo "New config file created: $output_file"

    # Make simulation output directory
    sim_output=$output_dir/sim_$line_number/sim_output
    mkdir -p $sim_output
    
    # Run simulation
    3dpolys_le_runner run -i $cfg_file -o $sim_output &> $output_dir/sim_$line_number/sim.log
    echo "Running simulation $output_dir/sim_$line_number"
    
    # Increment the line number
    ((line_number++))
done

echo "All config files created in '$output_dir'."
echo "All simulations running."

rm -f ./recombination.csv

